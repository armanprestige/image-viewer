#ifndef EVENTS_H
#define EVENTS_H

#include <QEvent>

enum class ImageViewerEvent
{
    ImageDataEvent = QEvent::Type::User + 1,
    ImageTaskEvent,
    ImageEventStarted,
    ImageEventFinished,
};

#endif // EVENTS_H
